package grpctool

import (
	"context"

	"google.golang.org/grpc"
)

// PoolSelf is a decorator that uses an in-memory connection to dial self rather than going over network.
type PoolSelf struct {
	delegate PoolInterface
	selfURL  string
	conn     selfPoolConn
}

func NewPoolSelf(delegate PoolInterface, selfURL string, selfConn grpc.ClientConnInterface) *PoolSelf {
	return &PoolSelf{
		delegate: delegate,
		selfURL:  selfURL,
		conn: selfPoolConn{
			ClientConnInterface: selfConn,
		},
	}
}

func (p *PoolSelf) Dial(ctx context.Context, targetURL string) (PoolConn, error) {
	if targetURL == p.selfURL {
		return &p.conn, nil
	}
	return p.delegate.Dial(ctx, targetURL)
}

func (p *PoolSelf) Close() error {
	return p.delegate.Close()
}

type selfPoolConn struct {
	grpc.ClientConnInterface
}

func (s *selfPoolConn) Done() {
	// noop
}
