package tunserver

import (
	"context"
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool/test"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap/zaptest"
)

const (
	selfAddr   = "grpc://self"
	kasURLPipe = "grpc://pipe"
)

func TestGatewayFinder_PollStartsSingleGoroutineForURL(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	tf, querier, api, kasPool := setupGatewayFinder(ctx, t)

	var wg sync.WaitGroup
	wg.Add(2)

	gomock.InOrder(
		kasPool.EXPECT().
			Dial(gomock.Any(), selfAddr).
			DoAndReturn(func(ctx context.Context, targetURL string) (grpctool.PoolConn, error) {
				wg.Done()
				<-ctx.Done() // block to simulate a long running dial
				return nil, ctx.Err()
			}),
		api.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), testhelpers.AgentID, gomock.Any(), gomock.Any()),
	)
	gomock.InOrder(
		querier.EXPECT().
			CachedGatewayURLs(testhelpers.AgentID),
		querier.EXPECT().
			PollGatewayURLs(gomock.Any(), testhelpers.AgentID, gomock.Any()).
			Do(func(ctx context.Context, agentID int64, cb PollGatewayURLsCallback) {
				cb([]string{kasURLPipe})
				cb([]string{kasURLPipe}) // same thing two times
				wg.Wait()
				cancel()
				<-ctx.Done()
			}),
	)
	gomock.InOrder(
		kasPool.EXPECT().
			Dial(gomock.Any(), kasURLPipe).
			DoAndReturn(func(ctx context.Context, targetURL string) (grpctool.PoolConn, error) {
				wg.Done()
				<-ctx.Done() // block to simulate a long running dial
				return nil, ctx.Err()
			}),
		api.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), testhelpers.AgentID, gomock.Any(), gomock.Any()),
	)

	_, err := tf.Find(ctx)
	assert.Same(t, context.Canceled, err)
	assert.Len(t, tf.connections, 2)
	assert.Contains(t, tf.connections, selfAddr)
	assert.Contains(t, tf.connections, kasURLPipe)
}

func TestGatewayFinder_PollStartsGoroutineForEachURL(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	tf, querier, api, kasPool := setupGatewayFinder(ctx, t)

	var wg sync.WaitGroup
	wg.Add(3)

	gomock.InOrder(
		kasPool.EXPECT().
			Dial(gomock.Any(), selfAddr).
			DoAndReturn(func(ctx context.Context, targetURL string) (grpctool.PoolConn, error) {
				wg.Done()
				<-ctx.Done() // block to simulate a long running dial
				return nil, ctx.Err()
			}),
		api.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), testhelpers.AgentID, gomock.Any(), gomock.Any()),
	)
	gomock.InOrder(
		querier.EXPECT().
			CachedGatewayURLs(testhelpers.AgentID),
		querier.EXPECT().
			PollGatewayURLs(gomock.Any(), testhelpers.AgentID, gomock.Any()).
			Do(func(ctx context.Context, agentID int64, cb PollGatewayURLsCallback) {
				cb([]string{kasURLPipe, "grpc://pipe2"})
				wg.Wait()
				cancel()
				<-ctx.Done()
			}),
	)
	kasPool.EXPECT().
		Dial(gomock.Any(), kasURLPipe).
		DoAndReturn(func(ctx context.Context, targetURL string) (grpctool.PoolConn, error) {
			wg.Done()
			<-ctx.Done() // block to simulate a long running dial
			return nil, ctx.Err()
		})
	kasPool.EXPECT().
		Dial(gomock.Any(), "grpc://pipe2").
		DoAndReturn(func(ctx context.Context, targetURL string) (grpctool.PoolConn, error) {
			wg.Done()
			<-ctx.Done() // block to simulate a long running dial
			return nil, ctx.Err()
		})
	api.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), testhelpers.AgentID, gomock.Any(), gomock.Any()).
		Times(2)
	_, err := tf.Find(ctx)
	assert.Same(t, context.Canceled, err)
	assert.Len(t, tf.connections, 3)
	assert.Contains(t, tf.connections, selfAddr)
	assert.Contains(t, tf.connections, kasURLPipe)
	assert.Contains(t, tf.connections, "grpc://pipe2")
}

func TestGatewayFinder_StopTryingAbsentKASURL(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	tf, querier, api, kasPool := setupGatewayFinder(ctx, t)

	var wg sync.WaitGroup
	wg.Add(2)

	gomock.InOrder(
		kasPool.EXPECT().
			Dial(gomock.Any(), selfAddr).
			DoAndReturn(func(ctx context.Context, targetURL string) (grpctool.PoolConn, error) {
				wg.Done()
				<-ctx.Done() // block to simulate a long running dial
				return nil, ctx.Err()
			}),
		api.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), testhelpers.AgentID, gomock.Any(), gomock.Any()),
	)
	gomock.InOrder(
		querier.EXPECT().
			CachedGatewayURLs(testhelpers.AgentID),
		querier.EXPECT().
			PollGatewayURLs(gomock.Any(), testhelpers.AgentID, gomock.Any()).
			Do(func(ctx context.Context, agentID int64, cb PollGatewayURLsCallback) {
				cb([]string{kasURLPipe})
				wg.Wait()
				cancel()
				<-ctx.Done()
			}),
	)
	kasPool.EXPECT().
		Dial(gomock.Any(), kasURLPipe).
		DoAndReturn(func(ctx context.Context, targetURL string) (grpctool.PoolConn, error) {
			defer wg.Done()
			tf.mu.Lock()
			defer tf.mu.Unlock()
			tf.gatewayURLs = nil // remove kasURLPipe from the list
			return nil, errors.New("boom")
		})
	api.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), testhelpers.AgentID, gomock.Any(), gomock.Any())
	_, err := tf.Find(ctx)
	assert.Same(t, context.Canceled, err)
	assert.Len(t, tf.connections, 1)
	assert.Contains(t, tf.connections, selfAddr)
}

func setupGatewayFinder(ctx context.Context, t *testing.T) (*gatewayFinder, *MockPollingGatewayURLQuerier, *mock_modshared.MockAPI, *mock_rpc.MockPoolInterface) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	querier := NewMockPollingGatewayURLQuerier(ctrl)
	agentFinder := NewMockAgentFinder(ctrl)
	api := mock_modshared.NewMockAPI(ctrl)
	kasPool := mock_rpc.NewMockPoolInterface(ctrl)

	agentFinder.EXPECT().AgentLastConnected(gomock.Any(), gomock.Any()).Return(time.Time{}, nil)

	tf := NewGatewayFinder(
		ctx,
		zaptest.NewLogger(t),
		kasPool,
		querier,
		agentFinder,
		api,
		test.Testing_RequestResponse_FullMethodName,
		selfAddr,
		testhelpers.AgentID,
		testhelpers.NewPollConfig(100*time.Millisecond),
		10*time.Millisecond,
	).(*gatewayFinder)
	return tf, querier, api, kasPool
}
