package tunserver

import (
	"context"
	"errors"
	"io"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"go.opentelemetry.io/otel/attribute"
	otelcodes "go.opentelemetry.io/otel/codes"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	_ grpc.ClientConnInterface = (*RoutingClientConn)(nil)
)

const (
	routingDurationMetricName                = "tunnel_routing_duration"
	routingTunnelTimeoutConnectedRecently    = "tunnel_routing_timeout_connected_recently_total"
	routingTunnelTimeoutNotConnectedRecently = "tunnel_routing_timeout_not_connected_recently_total"

	routerTracerName = "tunnel-router"

	routingStatusAttributeName attribute.Key = "status"
)

var (
	routingStatusSuccessAttrSet = attribute.NewSet(routingStatusAttributeName.String("success"))
	routingStatusAbortedAttrSet = attribute.NewSet(routingStatusAttributeName.String("aborted"))
)

type RoutingClientConn struct {
	log                                       *zap.Logger
	api                                       modshared.API
	plugin                                    RouterPlugin
	tracer                                    trace.Tracer
	routingDuration                           otelmetric.Float64Histogram
	routingTimeoutConnectedRecentlyCounter    otelmetric.Int64Counter
	routingTimeoutNotConnectedRecentlyCounter otelmetric.Int64Counter
	tunnelFindTimeout                         time.Duration
}

func NewRoutingClientConn(log *zap.Logger, api modshared.API, plugin RouterPlugin, tp trace.TracerProvider,
	dm otelmetric.Meter, tunnelFindTimeout time.Duration) (*RoutingClientConn, error) {
	routingDuration, routingTimeoutConnectedRecentlyCounter, routingTimeoutNotConnectedRecentlyCounter, err := constructRoutingMetrics(dm)
	if err != nil {
		return nil, err
	}
	return &RoutingClientConn{
		log:                                    log,
		api:                                    api,
		plugin:                                 plugin,
		tracer:                                 tp.Tracer(routerTracerName),
		routingDuration:                        routingDuration,
		routingTimeoutConnectedRecentlyCounter: routingTimeoutConnectedRecentlyCounter,
		routingTimeoutNotConnectedRecentlyCounter: routingTimeoutNotConnectedRecentlyCounter,
		tunnelFindTimeout:                         tunnelFindTimeout,
	}, nil
}

func (c *RoutingClientConn) Invoke(ctx context.Context, method string, args any, reply any, opts ...grpc.CallOption) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	stream, err := c.NewStream(ctx, nil, method, opts...)
	if err != nil {
		return err
	}
	err = stream.SendMsg(args)
	if err != nil {
		if err == io.EOF { //nolint:errorlint
			// It's useless to return the EOF error as the calling code has no means to get the actual server error.
			// So, while this behavior is different from gRPC, we do the right thing here - get and return the actual error.
			// See https://github.com/grpc/grpc-go/issues/7230 for more info.
			return stream.RecvMsg(reply) // return the actual underlying error
		}
		return err
	}
	err = stream.CloseSend() // We send close send even for unary RPCs.
	if err != nil {
		return err
	}
	err = stream.RecvMsg(reply)
	if err != nil {
		return err
	}
	var x any
	err = stream.RecvMsg(x) // consume EOF
	if err != io.EOF {      //nolint:errorlint
		return err
	}
	return nil
}

func (c *RoutingClientConn) NewStream(ctx context.Context, desc *grpc.StreamDesc, method string, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	// 0. process call options
	ro, err := newRoutingOpts(opts)
	if err != nil {
		return nil, err
	}

	// 1. find a ready, suitable gateway tunserver
	rg, log, agentID, err := c.findReadyGateway(ctx, c.log, method)
	if err != nil {
		return nil, err
	}

	// 2. start streaming via the found gateway tunserver
	return &routingStream{
		log:               log.With(logz.GatewayURL(rg.URL)),
		api:               c.api,
		ro:                ro,
		agentID:           agentID,
		destination:       rg.Stream,
		destinationCancel: rg.StreamCancel,
		codec:             rg.Codec,
	}, nil
}

func (c *RoutingClientConn) findReadyGateway(ctx context.Context, log *zap.Logger, method string) (ReadyGateway, *zap.Logger, int64, error) {
	startRouting := time.Now()
	findCtx, span := c.tracer.Start(ctx, "router.findReadyGateway", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	gf, log, agentID, err := c.plugin.GatewayFinder(ctx, log, method)
	if err != nil {
		return ReadyGateway{}, nil, 0, err
	}
	findCtx, findCancel := context.WithTimeout(findCtx, c.tunnelFindTimeout)
	defer findCancel()

	rg, err := gf.Find(findCtx)
	if err != nil {
		switch { // Order is important here.
		case ctx.Err() != nil: // Incoming stream canceled.
			c.routingDuration.Record( //nolint: contextcheck
				context.Background(),
				float64(time.Since(startRouting))/float64(time.Second),
				otelmetric.WithAttributeSet(routingStatusAbortedAttrSet),
			)
			span.SetStatus(otelcodes.Error, "Aborted")
			span.RecordError(ctx.Err())
			return ReadyGateway{}, nil, 0, grpctool.StatusErrorFromContext(ctx, "request aborted")
		case findCtx.Err() != nil: // Find tunnel timed out.
			findCtxErr := findCtx.Err()

			var targetErr *RecentlyConnectedAgentNotFound
			if errors.As(err, &targetErr) {
				c.routingTimeoutConnectedRecentlyCounter.Add(context.Background(), 1) //nolint: contextcheck
				span.SetStatus(otelcodes.Error, "Timed out, agent connected recently")
				span.RecordError(findCtxErr)

				c.api.HandleProcessingError(context.Background(), log, agentID, "Finding tunnel timed out. An agent recently connected", targetErr) //nolint:contextcheck
				return ReadyGateway{}, nil, 0, status.Errorf(codes.DeadlineExceeded, "finding tunnel timed out. An agent recently connected at %s", targetErr.LastConnectedAt)
			} else {
				c.routingTimeoutNotConnectedRecentlyCounter.Add(context.Background(), 1) //nolint: contextcheck
				span.SetStatus(otelcodes.Error, "Timed out, agent not connected recently")
				span.RecordError(findCtxErr)

				c.api.HandleProcessingError(context.Background(), log, agentID, "Finding tunnel timed out. An agent hasn't connected recently", errors.New(findCtxErr.Error())) //nolint:contextcheck
				return ReadyGateway{}, nil, 0, status.Error(codes.DeadlineExceeded, "finding tunnel timed out. An agent hasn't connected recently. Make sure the agent is connected and up to date")
			}
		default: // This should never happen, but let's handle a non-ctx error for completeness and future-proofing.
			span.SetStatus(otelcodes.Error, "Failed")
			span.RecordError(err)
			return ReadyGateway{}, nil, 0, status.Errorf(codes.Unavailable, "find tunnel failed: %v", err)
		}
	}
	c.routingDuration.Record( //nolint: contextcheck
		context.Background(),
		float64(time.Since(startRouting))/float64(time.Second),
		otelmetric.WithAttributeSet(routingStatusSuccessAttrSet),
	)
	span.SetStatus(otelcodes.Ok, "")
	return rg, log, agentID, nil
}

type routingOpts struct {
	header  []*metadata.MD
	trailer []*metadata.MD
}

func newRoutingOpts(opts []grpc.CallOption) (routingOpts, error) {
	r := routingOpts{}
	for _, opt := range opts {
		switch o := opt.(type) {
		case grpc.EmptyCallOption, grpc.StaticMethodCallOption:
		// ignore
		case grpc.HeaderCallOption:
			r.header = append(r.header, o.HeaderAddr)
		case grpc.TrailerCallOption:
			r.trailer = append(r.trailer, o.TrailerAddr)
		default:
			// We could support PeerCallOption and OnFinishCallOption, but we don't need them, so we don't.
			// PeerCallOption, FailFastCallOption, OnFinishCallOption, MaxRecvMsgSizeCallOption, MaxSendMsgSizeCallOption
			// PerRPCCredsCallOption, CompressorCallOption, ContentSubtypeCallOption, ForceCodecCallOption,
			// CustomCodecCallOption, MaxRetryRPCBufferSizeCallOption
			return routingOpts{}, status.Errorf(codes.Internal, "unsupported call option: %T", opt)
		}
	}
	return r, nil
}

func constructRoutingMetrics(dm otelmetric.Meter) (otelmetric.Float64Histogram, otelmetric.Int64Counter, otelmetric.Int64Counter, error) {
	hist, err := dm.Float64Histogram(
		routingDurationMetricName,
		otelmetric.WithUnit("s"),
		otelmetric.WithDescription("The time it takes the tunnel router to find a suitable tunnel in seconds"),
		otelmetric.WithExplicitBucketBoundaries(0.001, 0.004, 0.016, 0.064, 0.256, 1.024, 4.096, 16.384),
	)
	if err != nil {
		return nil, nil, nil, err
	}
	connectedTimeoutCounter, err := dm.Int64Counter(
		routingTunnelTimeoutNotConnectedRecently,
		otelmetric.WithDescription("The total number of times routing timed out for agents that are seemingly connected but no connection was found to use for the tunnel"),
	)
	if err != nil {
		return nil, nil, nil, err
	}
	registeredTimeoutCounter, err := dm.Int64Counter(
		routingTunnelTimeoutConnectedRecently,
		otelmetric.WithDescription("The total number of times routing timed out for agents that are registered but not connected"),
	)
	if err != nil {
		return nil, nil, nil, err
	}
	return hist, connectedTimeoutCounter, registeredTimeoutCounter, nil
}
