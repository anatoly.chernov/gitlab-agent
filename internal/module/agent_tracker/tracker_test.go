package agent_tracker //nolint:stylecheck

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/redistool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/matcher"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_redis"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_tool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/entity"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap/zaptest"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/testing/protocmp"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	_ ExpiringRegisterer         = &RedisTracker{}
	_ Querier                    = &RedisTracker{}
	_ Tracker                    = &RedisTracker{}
	_ ConnectedAgentInfoCallback = (&ConnectedAgentInfoCollector{}).Collect
)

func TestGC_HappyPath(t *testing.T) {
	r, connectedAgents, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, _, _ := setupTracker(t)

	wasCalled1 := false
	wasCalled2 := false
	wasCalled3 := false
	wasCalled4 := false
	wasCalled5 := false
	wasCalled6 := false

	connectedAgents.EXPECT().
		GC().
		Return(func(_ context.Context) (int, error) {
			wasCalled3 = true
			return 3, nil
		})

	byAgentID.EXPECT().
		GC().
		Return(func(_ context.Context) (int, error) {
			wasCalled2 = true
			return 2, nil
		})

	byProjectID.EXPECT().
		GC().
		Return(func(_ context.Context) (int, error) {
			wasCalled1 = true
			return 1, nil
		})

	gomock.InOrder(
		agentVersions.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled6 = true
				return 4, nil
			}),
		agentVersions.EXPECT().
			Scan(gomock.Any(), agentVersionKey, gomock.Any()).
			DoAndReturn(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
				wasCalled4 = true
				_, err := cb("v16.9.0", nil, nil)
				return err
			}),
		connectionsByAgentVersion.EXPECT().
			GCFor([]string{"v16.9.0"}).
			Return(func(_ context.Context) (int, error) {
				wasCalled5 = true
				return 5, nil
			}),
	)

	assert.EqualValues(t, 1+2+3+4+5, r.runGC(context.Background()))
	assert.True(t, wasCalled1)
	assert.True(t, wasCalled2)
	assert.True(t, wasCalled3)
	assert.True(t, wasCalled4)
	assert.True(t, wasCalled5)
	assert.True(t, wasCalled6)
}

func TestGC_AllCalledOnError(t *testing.T) {
	r, connectedAgents, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, rep, _ := setupTracker(t)

	wasCalled1 := false
	wasCalled2 := false
	wasCalled3 := false
	wasCalled4 := false
	wasCalled5 := false
	wasCalled6 := false

	gomock.InOrder(
		connectedAgents.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled3 = true
				return 3, errors.New("err3")
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in connected_agents Redis hash", matcher.ErrorEq("err3")),
	)

	gomock.InOrder(
		byAgentID.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled2 = true
				return 2, errors.New("err2")
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in connections_by_agent_id Redis hash", matcher.ErrorEq("err2")),
	)

	gomock.InOrder(
		byProjectID.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled1 = true
				return 1, errors.New("err1")
			}),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in connections_by_project_id Redis hash", matcher.ErrorEq("err1")),
	)

	err4 := errors.New("err4")
	err5 := errors.New("err5")
	gomock.InOrder(
		agentVersions.EXPECT().
			GC().
			Return(func(_ context.Context) (int, error) {
				wasCalled6 = true
				return 4, nil
			}),
		agentVersions.EXPECT().
			Scan(gomock.Any(), agentVersionKey, gomock.Any()).
			DoAndReturn(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
				wasCalled4 = true
				_, _ = cb("v16.9.0", nil, nil)
				return err4
			}),
		connectionsByAgentVersion.EXPECT().
			GCFor([]string{"v16.9.0"}).
			Return(func(_ context.Context) (int, error) {
				wasCalled5 = true
				return 5, err5
			}),

		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Failed to GC data in connections_by_agent_version Redis hash", errors.Join(err4, err5)),
	)

	assert.EqualValues(t, 1+2+3+4+5, r.runGC(context.Background()))
	assert.True(t, wasCalled1)
	assert.True(t, wasCalled2)
	assert.True(t, wasCalled3)
	assert.True(t, wasCalled4)
	assert.True(t, wasCalled5)
	assert.True(t, wasCalled6)
}

func TestGetConnectionsByProjectID_HappyPath(t *testing.T) {
	r, _, _, byProjectID, _, _, _, info := setupTracker(t)
	infoBytes, err := proto.Marshal(info)
	require.NoError(t, err)
	byProjectID.EXPECT().
		Scan(gomock.Any(), info.ProjectId, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
			done, cbErr := cb("k2", infoBytes, nil)
			require.NoError(t, cbErr)
			assert.False(t, done)
			return nil
		})
	var cbCalled int
	err = r.GetConnectionsByProjectID(context.Background(), info.ProjectId, func(i *ConnectedAgentInfo) (done bool, err error) {
		cbCalled++
		assert.Empty(t, cmp.Diff(i, info, protocmp.Transform()))
		return false, nil
	})
	require.NoError(t, err)
	assert.EqualValues(t, 1, cbCalled)
}

func TestGetConnectionsByProjectID_ScanError(t *testing.T) {
	r, _, _, byProjectID, _, _, rep, info := setupTracker(t)
	gomock.InOrder(
		byProjectID.EXPECT().
			Scan(gomock.Any(), info.ProjectId, gomock.Any()).
			Do(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
				done, err := cb("", nil, errors.New("intended error"))
				require.NoError(t, err)
				assert.False(t, done)
				return nil
			}),
		byProjectID.EXPECT().
			GetName().
			Return("connections_by_project_id"),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Redis connections_by_project_id hash scan", matcher.ErrorEq("intended error")),
	)
	err := r.GetConnectionsByProjectID(context.Background(), info.ProjectId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectionsByProjectID_UnmarshalError(t *testing.T) {
	r, _, _, byProjectID, _, _, rep, info := setupTracker(t)
	gomock.InOrder(
		byProjectID.EXPECT().
			Scan(gomock.Any(), info.ProjectId, gomock.Any()).
			Do(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
				done, err := cb("k2", []byte{1, 2, 3}, nil) // invalid bytes
				require.NoError(t, err)                     // ignores error to keep going
				assert.False(t, done)
				return nil
			}),
		byProjectID.EXPECT().
			GetName().
			Return("connections_by_project_id"),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Redis connections_by_project_id hash scan: proto.Unmarshal(ConnectedAgentInfo)", matcher.ErrorIs(proto.Error)),
	)
	err := r.GetConnectionsByProjectID(context.Background(), info.ProjectId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectionsByAgentID_HappyPath(t *testing.T) {
	r, _, byAgentID, _, _, _, _, info := setupTracker(t)
	infoBytes, err := proto.Marshal(info)
	require.NoError(t, err)
	byAgentID.EXPECT().
		Scan(gomock.Any(), info.AgentId, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
			var done bool
			done, err = cb("k2", infoBytes, nil)
			require.NoError(t, err)
			assert.False(t, done)
			return nil
		})
	var cbCalled int
	err = r.GetConnectionsByAgentID(context.Background(), info.AgentId, func(i *ConnectedAgentInfo) (done bool, err error) {
		cbCalled++
		assert.Empty(t, cmp.Diff(i, info, protocmp.Transform()))
		return false, nil
	})
	require.NoError(t, err)
	assert.EqualValues(t, 1, cbCalled)
}

func TestGetConnectionsByAgentID_ScanError(t *testing.T) {
	r, _, byAgentID, _, _, _, rep, info := setupTracker(t)
	gomock.InOrder(
		byAgentID.EXPECT().
			Scan(gomock.Any(), info.AgentId, gomock.Any()).
			Do(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
				done, err := cb("", nil, errors.New("intended error"))
				require.NoError(t, err)
				assert.False(t, done)
				return nil
			}),
		byAgentID.EXPECT().
			GetName().
			Return("connections_by_agent_id"),
		rep.EXPECT().
			HandleProcessingError(gomock.Any(), gomock.Any(), "Redis connections_by_agent_id hash scan", matcher.ErrorEq("intended error")),
	)
	err := r.GetConnectionsByAgentID(context.Background(), info.AgentId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectionsByAgentID_UnmarshalError(t *testing.T) {
	r, _, byAgentID, _, _, _, rep, info := setupTracker(t)
	byAgentID.EXPECT().
		Scan(gomock.Any(), info.AgentId, gomock.Any()).
		Do(func(ctx context.Context, key int64, cb redistool.ScanCallback) error {
			done, err := cb("k2", []byte{1, 2, 3}, nil) // invalid bytes
			require.NoError(t, err)                     // ignores error to keep going
			assert.False(t, done)
			return nil
		})
	byAgentID.EXPECT().
		GetName().
		Return("connections_by_agent_id")
	rep.EXPECT().
		HandleProcessingError(gomock.Any(), gomock.Any(), "Redis connections_by_agent_id hash scan: proto.Unmarshal(ConnectedAgentInfo)", matcher.ErrorIs(proto.Error))
	err := r.GetConnectionsByAgentID(context.Background(), info.AgentId, func(i *ConnectedAgentInfo) (done bool, err error) {
		require.FailNow(t, "unexpected call")
		return false, nil
	})
	require.NoError(t, err)
}

func TestGetConnectedAgentsCount_HappyPath(t *testing.T) {
	r, connectedAgents, _, _, _, _, _, _ := setupTracker(t)
	connectedAgents.EXPECT().
		Len(gomock.Any(), connectedAgentsKey).
		Return(int64(1), nil)
	size, err := r.GetConnectedAgentsCount(context.Background())
	require.NoError(t, err)
	assert.EqualValues(t, 1, size)
}

func TestGetConnectedAgentsCount_LenError(t *testing.T) {
	r, connectedAgents, _, _, _, _, _, _ := setupTracker(t)
	connectedAgents.EXPECT().
		Len(gomock.Any(), connectedAgentsKey).
		Return(int64(0), errors.New("intended error"))
	size, err := r.GetConnectedAgentsCount(context.Background())
	require.Error(t, err)
	assert.Zero(t, size)
}

func setupTracker(t *testing.T) (*RedisTracker, *mock_redis.MockExpiringHash[int64, int64],
	*mock_redis.MockExpiringHash[int64, int64], *mock_redis.MockExpiringHash[int64, int64],
	*mock_redis.MockExpiringHash[int64, string], *mock_redis.MockExpiringHash[string, int64],
	*mock_tool.MockErrReporter, *ConnectedAgentInfo) {
	ctrl := gomock.NewController(t)
	rep := mock_tool.NewMockErrReporter(ctrl)
	connectedAgents := mock_redis.NewMockExpiringHash[int64, int64](ctrl)
	byAgentID := mock_redis.NewMockExpiringHash[int64, int64](ctrl)
	byProjectID := mock_redis.NewMockExpiringHash[int64, int64](ctrl)
	agentVersions := mock_redis.NewMockExpiringHash[int64, string](ctrl)
	connectionsByAgentVersion := mock_redis.NewMockExpiringHash[string, int64](ctrl)
	tr := &RedisTracker{
		log:                       zaptest.NewLogger(t),
		errRep:                    rep,
		gcPeriod:                  time.Minute,
		connectionsByAgentID:      byAgentID,
		connectionsByProjectID:    byProjectID,
		connectedAgents:           connectedAgents,
		agentVersions:             agentVersions,
		connectionsByAgentVersion: connectionsByAgentVersion,
	}
	return tr, connectedAgents, byAgentID, byProjectID, agentVersions, connectionsByAgentVersion, rep, connInfo()
}

func connInfo() *ConnectedAgentInfo {
	return &ConnectedAgentInfo{
		AgentMeta: &entity.AgentMeta{
			Version:      "v1.2.3",
			CommitId:     "123123",
			PodNamespace: "ns",
			PodName:      "name",
		},
		ConnectedAt:  timestamppb.Now(),
		ConnectionId: 123,
		AgentId:      345,
		ProjectId:    456,
	}
}
