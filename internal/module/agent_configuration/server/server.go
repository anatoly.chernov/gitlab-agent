package server

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"path"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
	gapi "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/errz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/event"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
	"k8s.io/apimachinery/pkg/util/wait"
	"sigs.k8s.io/yaml"
)

type server struct {
	rpc.UnimplementedAgentConfigurationServer
	serverAPI                  modserver.API
	gitaly                     gitaly.PoolInterface
	gitLabClient               gitlab.ClientInterface
	maxConfigurationFileSize   int64
	getConfigurationPollConfig retry.PollConfigFactory
	gitLabExternalURL          string
}

func (s *server) GetConfiguration(req *rpc.ConfigurationRequest, server rpc.AgentConfiguration_GetConfigurationServer) error {
	ctx := server.Context()
	rpcAPI := modserver.AgentRPCAPIFromContext(ctx)
	log := rpcAPI.Log()

	pollCfg := s.getConfigurationPollConfig()

	wh := syncz.NewComparableWorkerHolder[string](
		func(projectID string) syncz.Worker {
			return syncz.WorkerFunc(func(ctx context.Context) {
				s.serverAPI.OnGitPushEvent(ctx, func(ctx context.Context, e *event.GitPushEvent) {
					// NOTE: yes, the req.ProjectId is NOT a project id, but a full project path ...
					if e.Project.FullPath == projectID {
						pollCfg.Poke()
					}
				})
			})
		},
	)
	defer wh.StopAndWait()

	lastProcessedCommitID := req.CommitId
	return rpcAPI.PollWithBackoff(pollCfg, func() (error, retry.AttemptResult) {
		// This call is made on each poll because:
		// - it checks that the agent's token is still valid
		// - repository location in Gitaly might have changed
		agentInfo, err := rpcAPI.AgentInfo(ctx, log)
		if err != nil {
			if status.Code(err) == codes.Unavailable {
				return nil, retry.Backoff
			}
			return err, retry.Done
		}
		wh.ApplyConfig(ctx, agentInfo.Repository.GlProjectPath)
		// re-define log to avoid accidentally using the old one
		log := log.With(logz.AgentID(agentInfo.ID), logz.ProjectID(agentInfo.Repository.GlProjectPath)) //nolint:govet
		info, err := s.poll(ctx, agentInfo, lastProcessedCommitID)
		if err != nil {
			switch gitaly.ErrorCodeFromError(err) { //nolint:exhaustive
			case gitaly.NotFound: // ref not found
				return status.Errorf(codes.NotFound, "config: repository poll failed: %v", err), retry.Done
			default:
				rpcAPI.HandleProcessingError(log, agentInfo.ID, "config: repository poll failed", err)
				return nil, retry.Backoff
			}
		}
		if info.RefNotFound {
			log.Debug("Config: ref not found")
			return nil, retry.Continue
		}
		if !info.UpdateAvailable {
			log.Debug("Config: no updates", logz.CommitID(lastProcessedCommitID))
			return nil, retry.Continue
		}
		log.Info("Config: new commit", logz.CommitID(info.CommitID))
		configFile, err := s.fetchConfiguration(ctx, agentInfo, info.CommitID)
		if err != nil {
			rpcAPI.HandleProcessingError(log, agentInfo.ID, "Config: failed to fetch", err)
			var ue errz.UserError
			if errors.As(err, &ue) {
				// return the error to the client because it's a user error
				return status.Errorf(codes.FailedPrecondition, "config: %v", err), retry.Done
			}
			return nil, retry.Backoff
		}
		var wg wait.Group
		defer wg.Wait()
		wg.Start(func() {
			err := gapi.PostAgentConfiguration(ctx, s.gitLabClient, agentInfo.ID, configFile) //nolint:govet
			switch {
			case err == nil:
			case gitlab.IsNotFound(err):
				// Agent has been deleted from DB, but it's still running in the cluster. Don't need to send this error
				// to Sentry.
				log.Debug("Failed to notify GitLab of new agent configuration. Deleted agent?", logz.Error(err))
			default:
				rpcAPI.HandleProcessingError(log, agentInfo.ID, "Failed to notify GitLab of new agent configuration", err)
			}
		})
		err = s.sendConfigResponse(server, agentInfo, configFile, info.CommitID)
		if err != nil {
			return rpcAPI.HandleIOError(log, "config: failed to send config", err), retry.Done
		}
		lastProcessedCommitID = info.CommitID
		return nil, retry.Continue
	})
}

func (s *server) poll(ctx context.Context, agentInfo *api.AgentInfo, lastProcessedCommitID string) (*gitaly.PollInfo, error) {
	p, err := s.gitaly.Poller(ctx, agentInfo.GitalyInfo)
	if err != nil {
		return nil, err
	}
	return p.Poll(ctx, agentInfo.Repository, lastProcessedCommitID, "refs/heads/"+agentInfo.DefaultBranch)
}

func (s *server) sendConfigResponse(server rpc.AgentConfiguration_GetConfigurationServer,
	agentInfo *api.AgentInfo, configFile *agentcfg.ConfigurationFile, commitID string) error {
	return server.Send(&rpc.ConfigurationResponse{
		Configuration: &agentcfg.AgentConfiguration{
			Gitops:            configFile.Gitops,
			Observability:     configFile.Observability,
			AgentId:           agentInfo.ID,
			ProjectId:         agentInfo.ProjectID,
			ProjectPath:       agentInfo.Repository.GlProjectPath,
			CiAccess:          configFile.CiAccess,
			ContainerScanning: configFile.ContainerScanning,
			RemoteDevelopment: configFile.RemoteDevelopment,
			Flux:              configFile.Flux,
			GitlabExternalUrl: s.gitLabExternalURL,
		},
		CommitId: commitID,
	})
}

// fetchConfiguration fetches agent's configuration from a corresponding repository.
// Assumes configuration is stored in ".gitlab/agents/<agent id>/config.yaml" file.
// fetchConfiguration returns a wrapped context.Canceled, context.DeadlineExceeded or gRPC error if ctx signals done and interrupts a running gRPC call.
func (s *server) fetchConfiguration(ctx context.Context, agentInfo *api.AgentInfo, commitID string) (*agentcfg.ConfigurationFile, error) {
	pf, err := s.gitaly.PathFetcher(ctx, agentInfo.GitalyInfo)
	if err != nil {
		return nil, fmt.Errorf("PathFetcher: %w", err) // wrap
	}
	filename := path.Join(agent_configuration.Directory, agentInfo.Name, agent_configuration.FileName)
	configYAML, err := pf.FetchFile(ctx, agentInfo.Repository, []byte(commitID), []byte(filename), s.maxConfigurationFileSize)
	if err != nil {
		switch gitaly.ErrorCodeFromError(err) { //nolint:exhaustive
		case gitaly.NotFound:
			configYAML = nil // Missing config is the same as empty config
		case gitaly.FileTooBig, gitaly.UnexpectedTreeEntryType:
			return nil, errz.NewUserErrorWithCause(err, "agent configuration file")
		default:
			return nil, fmt.Errorf("fetch agent configuration: %w", err) // wrap
		}
	}
	configFile, err := parseYAMLToConfiguration(configYAML)
	if err != nil {
		return nil, errz.NewUserErrorWithCause(err, "failed to parse agent configuration")
	}
	err = configFile.ValidateAll()
	if err != nil {
		return nil, errz.NewUserErrorWithCause(err, "invalid agent configuration")
	}
	return configFile, nil
}

func parseYAMLToConfiguration(configYAML []byte) (*agentcfg.ConfigurationFile, error) {
	configJSON, err := yaml.YAMLToJSON(configYAML)
	if err != nil {
		return nil, fmt.Errorf("YAMLToJSON: %w", err)
	}
	configFile := &agentcfg.ConfigurationFile{}
	if bytes.Equal(configJSON, []byte("null")) {
		// Empty config
		return configFile, nil
	}
	err = protojson.Unmarshal(configJSON, configFile)
	if err != nil {
		return nil, fmt.Errorf("protojson.Unmarshal: %w", err)
	}
	return configFile, nil
}
