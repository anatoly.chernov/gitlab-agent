package router

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type pollingState byte

const (
	stopped pollingState = iota
	running
)

type pollConsumer struct {
	ctxDone  <-chan struct{}
	kasURLsC chan<- []string
}

type pollingContext struct {
	mu        sync.Mutex
	consumers map[*pollConsumer]struct{}
	cancel    context.CancelFunc
	kasURLs   []string
	stoppedAt time.Time
	state     pollingState
}

func newPollingContext() *pollingContext {
	return &pollingContext{
		consumers: map[*pollConsumer]struct{}{},
		state:     stopped,
	}
}

func (c *pollingContext) copyConsumersInto(consumers []pollConsumer) []pollConsumer {
	consumers = consumers[:0]
	c.mu.Lock()
	defer c.mu.Unlock()
	for h := range c.consumers {
		consumers = append(consumers, *h)
	}
	return consumers
}

func (c *pollingContext) setKASURLs(kasURLs []string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.kasURLs = kasURLs
}

func (c *pollingContext) isExpired(before time.Time) bool {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.state == stopped && c.stoppedAt.Before(before)
}

// AggregatingQuerier groups polling requests.
type AggregatingQuerier struct {
	log        *zap.Logger
	delegate   Querier
	api        modshared.API
	tracer     trace.Tracer
	pollConfig retry.PollConfigFactory
	gcPeriod   time.Duration

	mu        sync.Mutex
	listeners map[int64]*pollingContext
}

func NewAggregatingQuerier(log *zap.Logger, delegate Querier, api modshared.API, tracer trace.Tracer, pollConfig retry.PollConfigFactory, gcPeriod time.Duration) *AggregatingQuerier {
	return &AggregatingQuerier{
		log:        log,
		delegate:   delegate,
		api:        api,
		tracer:     tracer,
		pollConfig: pollConfig,
		gcPeriod:   gcPeriod,
		listeners:  make(map[int64]*pollingContext),
	}
}

func (q *AggregatingQuerier) Run(ctx context.Context) error {
	done := ctx.Done()
	t := time.NewTicker(q.gcPeriod)
	defer t.Stop()
	for {
		select {
		case <-done:
			return nil
		case <-t.C:
			q.runGC()
		}
	}
}

func (q *AggregatingQuerier) runGC() {
	before := time.Now().Add(-q.gcPeriod)
	q.mu.Lock()
	defer q.mu.Unlock()
	for agentID, pc := range q.listeners {
		if pc.isExpired(before) {
			delete(q.listeners, agentID)
		}
	}
}

func (q *AggregatingQuerier) PollGatewayURLs(ctx context.Context, agentID int64, cb tunserver.PollGatewayURLsCallback) {
	kasURLsC := make(chan []string)
	ctxDone := ctx.Done()
	h := &pollConsumer{
		ctxDone:  ctxDone,
		kasURLsC: kasURLsC,
	}
	q.maybeStartPolling(agentID, h) //nolint: contextcheck
	defer q.maybeStopPolling(agentID, h)
	for {
		select {
		case <-ctxDone:
			return
		case kasURLs := <-kasURLsC:
			cb(kasURLs)
		}
	}
}

func (q *AggregatingQuerier) CachedGatewayURLs(agentID int64) []string {
	q.mu.Lock()
	defer q.mu.Unlock()
	pc := q.listeners[agentID]
	if pc == nil { // no existing context
		return nil
	}
	pc.mu.Lock()
	defer pc.mu.Unlock()
	return pc.kasURLs
}

func (q *AggregatingQuerier) maybeStartPolling(agentID int64, h *pollConsumer) {
	q.mu.Lock()
	defer q.mu.Unlock()
	pc := q.listeners[agentID]
	if pc == nil { // no existing context
		pc = newPollingContext()
		q.listeners[agentID] = pc
	}
	q.registerConsumerLocked(agentID, pc, h)
}

func (q *AggregatingQuerier) registerConsumerLocked(agentID int64, pc *pollingContext, h *pollConsumer) {
	pc.mu.Lock()
	defer pc.mu.Unlock()
	pc.consumers[h] = struct{}{} // register for notifications
	switch pc.state {
	case stopped:
		pc.state = running
		ctx, cancel := context.WithCancel(context.Background())
		pc.cancel = cancel
		go q.poll(ctx, agentID, pc)
	case running:
		// Already polling, nothing to do.
	default:
		panic(fmt.Sprintf("invalid State value: %d", pc.state))
	}
}

func (q *AggregatingQuerier) maybeStopPolling(agentID int64, h *pollConsumer) {
	q.mu.Lock()
	defer q.mu.Unlock()

	pc := q.listeners[agentID]
	if q.unregisterConsumerLocked(pc, h) {
		// No point in keeping this pollingContext around if it doesn't have any cached URLs.
		delete(q.listeners, agentID)
	}
}

func (q *AggregatingQuerier) unregisterConsumerLocked(pc *pollingContext, h *pollConsumer) bool {
	pc.mu.Lock()
	defer pc.mu.Unlock()

	delete(pc.consumers, h)
	if len(pc.consumers) == 0 {
		pc.cancel()     // stop polling
		pc.cancel = nil // release the kraken! err... GC
		pc.state = stopped
		pc.stoppedAt = time.Now()
		return len(pc.kasURLs) == 0
	}
	return false
}

func (q *AggregatingQuerier) poll(ctx context.Context, agentID int64, pc *pollingContext) {
	ctx, span := q.tracer.Start(ctx, "AggregatingQuerier.poll",
		trace.WithSpanKind(trace.SpanKindInternal),
		trace.WithAttributes(api.TraceAgentIDAttr.Int64(agentID)),
	)
	defer span.End()

	var consumers []pollConsumer // reuse slice between polls
	_ = retry.PollWithBackoff(ctx, q.pollConfig(), func(ctx context.Context) (error, retry.AttemptResult) {
		kasURLs, err := q.delegate.KASURLsByAgentID(ctx, agentID)
		if err != nil {
			q.api.HandleProcessingError(ctx, q.log, agentID, "KASURLsByAgentID() failed", err)
			// fallthrough
		}
		if len(kasURLs) > 0 {
			consumers = pc.copyConsumersInto(consumers)
			for _, h := range consumers {
				select {
				case <-h.ctxDone:
					// This PollGatewayURLs() invocation is no longer interested in being called. Ignore it.
				case h.kasURLsC <- kasURLs:
					// Data sent.
				}
			}
		}
		if err != nil && len(kasURLs) == 0 {
			// if there was an error, and we failed to retrieve any kas URLs from Redis, we don't want to erase the
			// cache. So, no-op here.
		} else {
			pc.setKASURLs(kasURLs)
		}
		return nil, retry.Continue
	})
}
