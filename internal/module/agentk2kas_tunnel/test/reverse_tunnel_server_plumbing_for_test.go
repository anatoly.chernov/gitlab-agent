package test

import (
	"context"
	"net"
	"testing"
	"time"

	"github.com/ash2k/stager"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/validator"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/router"
	agentk2kas_tunnel_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_agentk2kas_router"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
	"go.opentelemetry.io/otel/trace/noop"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/durationpb"
)

func serverConstructComponents(ctx context.Context, t *testing.T) (func(context.Context) error, *grpc.ClientConn, *grpc.ClientConn, *mock_modserver.MockAgentRPCAPI, *mock_agentk2kas_router.MockTracker) {
	log := zaptest.NewLogger(t)
	ctrl := gomock.NewController(t)
	mockAPI := mock_modserver.NewMockAPI(ctrl)
	serverRPCAPI := mock_modserver.NewMockAgentRPCAPI(ctrl)
	serverRPCAPI.EXPECT().
		Log().
		Return(log).
		AnyTimes()
	serverRPCAPI.EXPECT().
		PollWithBackoff(gomock.Any(), gomock.Any()).
		DoAndReturn(func(cfg retry.PollConfig, f retry.PollWithBackoffFunc) error {
			for {
				err, result := f()
				if result == retry.Done {
					return err
				}
			}
		}).
		MinTimes(1)

	tunnelTracker := mock_agentk2kas_router.NewMockTracker(ctrl)
	agentServer := serverConstructAgentServer(ctx, serverRPCAPI)
	agentServerListener := grpctool.NewDialListener()

	internalListener := grpctool.NewDialListener()
	tr := noop.NewTracerProvider().Tracer("test")
	tunnelRegistry := router.NewRegistry(log, mockAPI, tr, time.Minute, time.Minute, time.Minute, tunnelTracker)

	internalServer := serverConstructInternalServer(ctx, log)
	internalServerConn, err := serverConstructInternalServerConn(internalListener.DialContext)
	require.NoError(t, err)

	serverFactory := agentk2kas_tunnel_server.Factory{
		TunnelHandler: tunnelRegistry,
	}
	serverConfig := &modserver.Config{
		Log: log,
		Config: &kascfg.ConfigurationFile{
			Agent: &kascfg.AgentCF{
				Listen: &kascfg.ListenAgentCF{
					MaxConnectionAge: durationpb.New(time.Minute),
				},
			},
		},
		AgentServer: agentServer,
		AgentConnPool: func(agentID int64) grpc.ClientConnInterface {
			return internalServerConn
		},
	}
	serverModule, err := serverFactory.New(serverConfig)
	require.NoError(t, err)

	kasConn, err := serverConstructKASConnection(testhelpers.AgentkToken, agentServerListener.DialContext)
	require.NoError(t, err)

	registerTestingServer(internalServer, &serverTestingServer{
		registry: tunnelRegistry,
	})

	return func(ctx context.Context) error {
		return stager.RunStages(ctx,
			// Start modules.
			func(stage stager.Stage) {
				if serverModule != nil {
					stage.Go(serverModule.Run)
				}
			},
			// Start gRPC servers.
			func(stage stager.Stage) {
				serverStartAgentServer(stage, agentServer, agentServerListener)
				serverStartInternalServer(stage, internalServer, internalListener)
			},
		)
	}, kasConn, internalServerConn, serverRPCAPI, tunnelTracker
}

func serverConstructInternalServer(ctx context.Context, log *zap.Logger) *grpc.Server {
	_, sh := grpctool.MaxConnectionAge2GRPCKeepalive(ctx, time.Minute)
	factory := func(ctx context.Context, fullMethodName string) modshared.RPCAPI {
		return &serverRPCAPIForTest{
			RPCAPIStub: modshared.RPCAPIStub{
				StreamCtx: ctx,
				Logger:    log,
			},
		}
	}
	return grpc.NewServer(
		grpc.StatsHandler(sh),
		grpc.ForceServerCodec(grpctool.RawCodec{}),
		grpc.ChainStreamInterceptor(
			modshared.StreamRPCAPIInterceptor(factory),
		),
		grpc.ChainUnaryInterceptor(
			modshared.UnaryRPCAPIInterceptor(factory),
		),
	)
}

func serverConstructInternalServerConn(dialContext func(ctx context.Context, addr string) (net.Conn, error)) (*grpc.ClientConn, error) {
	return grpc.NewClient("passthrough:internal-server",
		grpc.WithContextDialer(dialContext),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainStreamInterceptor(
			grpctool.StreamClientValidatingInterceptor,
		),
		grpc.WithChainUnaryInterceptor(
			grpctool.UnaryClientValidatingInterceptor,
		),
	)
}

func serverConstructKASConnection(agentToken api.AgentToken, dialContext func(ctx context.Context, addr string) (net.Conn, error)) (*grpc.ClientConn, error) {
	return grpc.NewClient("passthrough:conn-to-kas",
		grpc.WithContextDialer(dialContext),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(grpctool.NewTokenCredentials(agentToken, true)),
		grpc.WithChainStreamInterceptor(
			grpctool.StreamClientValidatingInterceptor,
		),
		grpc.WithChainUnaryInterceptor(
			grpctool.UnaryClientValidatingInterceptor,
		),
	)
}

func serverStartInternalServer(stage stager.Stage, internalServer *grpc.Server, internalListener net.Listener) {
	grpctool.StartServer(stage, internalServer, func() (net.Listener, error) {
		return internalListener, nil
	}, func() {})
}

func serverConstructAgentServer(ctx context.Context, rpcAPI modserver.AgentRPCAPI) *grpc.Server {
	kp, sh := grpctool.MaxConnectionAge2GRPCKeepalive(ctx, time.Minute)
	factory := func(ctx context.Context, fullMethodName string) (modserver.AgentRPCAPI, error) {
		return rpcAPI, nil
	}
	return grpc.NewServer(
		grpc.StatsHandler(sh),
		kp,
		grpc.ChainStreamInterceptor(
			grpc_validator.StreamServerInterceptor(),
			modserver.StreamAgentRPCAPIInterceptor(factory),
		),
		grpc.ChainUnaryInterceptor(
			grpc_validator.UnaryServerInterceptor(),
			modserver.UnaryAgentRPCAPIInterceptor(factory),
		),
	)
}

func serverStartAgentServer(stage stager.Stage, agentServer *grpc.Server, agentServerListener net.Listener) {
	grpctool.StartServer(stage, agentServer, func() (net.Listener, error) {
		return agentServerListener, nil
	}, func() {})
}

type serverRPCAPIForTest struct {
	modshared.RPCAPIStub
}

func (a *serverRPCAPIForTest) HandleProcessingError(log *zap.Logger, agentID int64, msg string, err error) {
	log.Error(msg, logz.Error(err))
}

func (a *serverRPCAPIForTest) HandleIOError(log *zap.Logger, msg string, err error) error {
	log.Debug(msg, logz.Error(err))
	return grpctool.HandleIOError(msg, err)
}
