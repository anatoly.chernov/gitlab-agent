package agent

import (
	"context"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"go.uber.org/zap"
)

// worker is responsible for coordinating and executing full and
// partial reconciliations by managing the lifecycle of a reconciler
type worker struct {
	log                           *zap.Logger
	api                           modagent.API
	fullReconciliationInterval    time.Duration
	partialReconciliationInterval time.Duration
	reconcilerFactory             func(ctx context.Context) (remoteDevReconciler, error)
}

func (w *worker) Run(ctx context.Context) error {
	agentID, err := w.api.GetAgentID(ctx)
	if err != nil {
		return err
	}

	// full reconciliation should be started immediately
	// upon module start/restart
	fullReconciliationTimer := time.NewTimer(0)
	defer fullReconciliationTimer.Stop()

	partialReconciliationTimer := time.NewTimer(w.partialReconciliationInterval)
	defer partialReconciliationTimer.Stop()

	var (
		activeReconciler remoteDevReconciler
	)
	defer func() {
		// this nil check is needed in case the context is canceled before the first
		// full reconciliation has even been scheduled to execute. In such a case, the
		// active reconciler will still be nil and the call to Stop may be skipped
		if activeReconciler != nil {
			activeReconciler.Stop()
		}
	}()

	done := ctx.Done()
	for {
		select {
		// this check allows the goroutine to immediately exit
		// if the context cancellation is invoked while waiting on
		// either of the timers
		case <-done:
			return nil

		case <-fullReconciliationTimer.C:
			// full reconciliation is implemented by creating a new reconciler
			// while discarding the state accrued in the previous reconciler
			w.log.Info("starting full reconciliation")
			if activeReconciler != nil {
				activeReconciler.Stop()
			}

			// Full reconciliation could have been alternatively implemented by re-using a reconciler vs
			// the current approach of destroying the existing reconciler and creating a new one
			// This has been done for the following reasons
			//  1. The current approach of stopping/starting a reconciler is conceptually
			//		equivalent to a module restart with minimal changes to the reconciliation logic/code.
			//		Full reconciliation implemented with reconciler re-use would've required introducing special handling in the
			// 		reconciliation code. This means that any future updates to the reconciliation logic would've
			//		led to increased complexity due to possible impact on full & partial reconciliation logic
			//		leading to increased maintenance cost.
			//  2. Reconciler re-use is inadequate when dealing with issues that occur due to corruption/
			//		mishandling of internal state, for example memory leaks that may occur due to bugs in
			//		reconciliation logic. The current approach will be able to deal with these as the core logic
			// 		requires teardown of the active reconciler and creation of a new one
			activeReconciler, err = w.reconcilerFactory(ctx)
			if err != nil {
				return err
			}
			execError := activeReconciler.Run(ctx)
			if execError != nil {
				w.api.HandleProcessingError(
					ctx, w.log, agentID,
					"Remote Dev - full reconciliation cycle ended with error", execError,
				)
			}

			// Timer is reset after the work has been completed
			// If the timer were reset before reconciliation is executed, there may be a scenario
			// where the next timer tick occurs immediately after the reconciler finishes its
			// execution (because Run() takes too long for some reason)
			fullReconciliationTimer.Reset(w.fullReconciliationInterval)

		case <-partialReconciliationTimer.C:
			w.log.Info("starting partial update")
			execError := activeReconciler.Run(ctx)
			if execError != nil {
				w.api.HandleProcessingError(
					ctx, w.log, agentID,
					"Remote Dev - partial reconciliation cycle ended with error", execError,
				)
			}

			// Timer is reset after the work has been completed
			// If the timer were reset before reconciler is executed, there may be a scenario
			// where the next timer tick occurs immediately after the reconciler finishes its
			// execution (because Run() takes too long for some reason)
			partialReconciliationTimer.Reset(w.partialReconciliationInterval)
		}
	}
}
