package agentkapp

import (
	"context"
	"errors"
	"net"

	"github.com/ash2k/stager"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/validator"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// inMemAPIServer represents agentk API that kas can talk to when running in agentk->kas tunnel mode.
type inMemAPIServer struct {
	server        *grpc.Server
	inMemConn     *grpc.ClientConn
	inMemListener net.Listener
}

func newInMemAPIServer(log *zap.Logger, ot *obsTools) (*inMemAPIServer, error) {
	// In-mem gRPC client->listener pipe
	listener := grpctool.NewDialListener()

	// Construct connection to the API gRPC server
	conn, err := grpc.NewClient("passthrough:api-server",
		grpc.WithSharedWriteBuffer(true),
		grpc.WithContextDialer(listener.DialContext),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultCallOptions(grpc.ForceCodec(grpctool.RawCodec{})),
	)
	if err != nil {
		return nil, err
	}
	factory := func(ctx context.Context, method string) modshared.RPCAPI {
		service, method := grpctool.SplitGRPCMethod(method)
		return &agentRPCAPI{
			RPCAPIStub: modshared.RPCAPIStub{
				Logger:    log.With(logz.TraceIDFromContext(ctx), logz.GRPCService(service), logz.GRPCMethod(method)),
				StreamCtx: ctx,
			},
		}
	}
	return &inMemAPIServer{
		server: grpc.NewServer(
			grpc.StatsHandler(otelgrpc.NewServerHandler(
				otelgrpc.WithTracerProvider(ot.tp),
				otelgrpc.WithMeterProvider(ot.mp),
				otelgrpc.WithPropagators(ot.p),
				otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
			)),
			grpc.StatsHandler(grpctool.ServerNoopMaxConnAgeStatsHandler{}),
			grpc.SharedWriteBuffer(true),
			grpc.ChainStreamInterceptor(
				ot.streamProm, // 1. measure all invocations
				modshared.StreamRPCAPIInterceptor(factory), // 2. inject RPC API
				grpc_validator.StreamServerInterceptor(),   // x. wrap with validator
			),
			grpc.ChainUnaryInterceptor(
				ot.unaryProm, // 1. measure all invocations
				modshared.UnaryRPCAPIInterceptor(factory), // 2. inject RPC API
				grpc_validator.UnaryServerInterceptor(),   // x. wrap with validator
			),
		),
		inMemConn:     conn,
		inMemListener: listener,
	}, nil
}

func (s *inMemAPIServer) Start(stage stager.Stage) {
	grpctool.StartServer(stage, s.server, func() (net.Listener, error) {
		return s.inMemListener, nil
	}, func() {})
}

func (s *inMemAPIServer) Close() error {
	return errors.Join(
		s.inMemConn.Close(),     // first close the client
		s.inMemListener.Close(), // then close the listener (if not closed already)
	)
}
