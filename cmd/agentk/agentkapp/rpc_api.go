package agentkapp

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"go.uber.org/zap"
)

var (
	_ modshared.RPCAPI = (*agentRPCAPI)(nil)
)

type agentRPCAPI struct {
	modshared.RPCAPIStub
}

func (a *agentRPCAPI) HandleProcessingError(log *zap.Logger, agentID int64, msg string, err error) {
	handleProcessingError(a.StreamCtx, log, agentID, msg, err)
}

func (a *agentRPCAPI) HandleIOError(log *zap.Logger, msg string, err error) error {
	// The problem is almost certainly with the client's connection.
	// Still log it on Debug.
	log.Debug(msg, logz.Error(err))
	return grpctool.HandleIOError(msg, err)
}
