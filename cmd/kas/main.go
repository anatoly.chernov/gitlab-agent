package main

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd/kas/kasapp"
)

func main() {
	cmd.Run(kasapp.NewCommand())
}
